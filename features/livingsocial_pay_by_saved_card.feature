########################################################################
# Before running the calabash tests, something need to be done first.
# 1. Make sure the vertical name and deal name has been provided.
# 2. Make sure the test account has at least one saved shipping address.
# 3. Make sure the test account and password have been provided.
########################################################################
# Feature: Test the checkout with saved card
#     As a Consumer
#     I want to buy a deal and select a saved card during Checkout Process
#     So I can make Purchases
# 
#   Scenario: Buy a PSS deal (Product)
#     When there_is_a_update_dialog_dismiss_it
#  	When I_am_authenticated
#    	Then I_clear_the_item_from_my_cart
#    	
#     #The name of vertical
#  	Then I_select_vertical "Shop"
#  	
#     #The name of deal
#  	Then I_scroll_to_deal "Prada Sunglasses"
#  	
#     #The name of deal
#  	Then I_select_deal "Prada Sunglasses"
#  	Then I_select_a_product
# 
# 	Then I_add_item_to_cart
# 	Then I_go_to_my_cart
# 	Then I_start_making_payment
#  	Then I_select_address_if_needed
#     Then I_pay_by_saved_card cvc "000" 


#   Scenario: Buy a multi-price deal 
#     When there_is_a_update_dialog_dismiss_it
#    	When I_am_authenticated
#    	Then I_clear_the_item_from_my_cart
# 
#    	#The name of vertical
#  	Then I_select_vertical "Shop"
# 
#  	#The name of deal
#  	Then I_scroll_to_deal "Wonder Knife"
# 
# 	#The name of deal
#  	Then I_select_deal "Wonder Knife"
#  	Then I_wait_deal_can_buy
# 
# 	Then I_add_item_to_cart
# 	Then I_select_first_price_option
# 	Then I_go_to_my_cart
# 
# 	Then I_start_making_payment
#  	Then I_select_address_if_needed
#     Then I_pay_by_saved_card cvc "000" 
# 
# 
#   Scenario: Buy a multiprice deal with option
#     When there_is_a_update_dialog_dismiss_it
#    	When I_am_authenticated
#    	Then I_clear_the_item_from_my_cart
# 
#    	#The name of vertical
#  	Then I_select_vertical "Local"
# 
#  	#The name of deal
#  	Then I_scroll_to_deal "AUTO-TEST PSS deal"
# 
# 	#The name of deal
#  	Then I_select_deal "AUTO-TEST PSS deal"
#  	Then I_wait_deal_can_buy
# 
# 	Then I_add_item_to_cart
# 	Then I_select_first_price_option
# 	Then I_select_a_parameter_if_needed
# 
# 	Then I_go_to_my_cart
# 	Then I_start_making_payment
#  	Then I_select_address_if_needed
#     Then I_pay_by_saved_card cvc "000" 
#  
#   Scenario: Buy a multiprice deal with location
#     When there_is_a_update_dialog_dismiss_it
#    	When I_am_authenticated
#    	Then I_clear_the_item_from_my_cart
# 
#    	#The name of vertical
#  	Then I_select_vertical "Local"
# 
#  	#The name of deal
#  	Then I_scroll_to_deal "AUTO-TEST Test multiprice with location"
# 
# 	#The name of deal
#  	Then I_select_deal "AUTO-TEST Test multiprice with location"
#  	Then I_wait_deal_can_buy
# 
# 	Then I_add_item_to_cart
# 	Then I_select_second_price_option
# 	Then I_select_location	
# 
# 	Then I_go_to_my_cart
# 	Then I_start_making_payment
#     Then I_pay_by_saved_card cvc "000"

  