Then /^I swipe long right$/ do
  perform_action('drag_coordinates', 900, 300, 100, 300, 10, 0.5, 0.5)
end

Then /^I swipe long left$/ do
  perform_action('drag_coordinates', 100, 300, 900, 300, 10, 0.5, 0.5)
end

Then /^I scroll slow down$/ do
  perform_action('drag_coordinates', 150, 700, 150, 200, 10, 0.5, 0.5)
end

Then /^I scroll slow up$/ do
  perform_action('drag_coordinates', 150, 400, 150, 900, 10, 0.5, 0.5)
end

###########################################################################
#   					Methods for login and logout					  #
###########################################################################
When /^I_am_authenticated$/ do
# 	step %{I wait for 1 second}
    sleep(2)
	step %{I press "Open navigation drawer"}
	step %{I see "FAQs"}
	step %{I press "My Account"}
# 	step %{I wait for 5 seconds}
    sleep(5)
	login_button = query("* id:'dialog_option_login'")
	if login_button.length > 0
		puts "Need login first"
		step %{I press "dialog_option_login"}
# 		step %{I wait for 3 seconds}
		sleep(3)
		step %{I see "Login"}
		step %{I enter "hai2@huang.com" into "Email"}
		step %{I enter "123456" into "password"}
		step %{I press "btn_login"}
# 		step %{I wait for 5 seconds}
		sleep(5)
	else
		puts "You already logged in"
		step %{I go back}
		step %{I wait for 2 seconds}
		sleep(5)
	end
end

# Then /^I_sign_up_an_test_account$/ do
# 	step %{I wait for 1 second}
# 	step %{I press "Open navigation drawer"}
# 	step %{I see "FAQs"}
# 	step %{I press "My Account"}
# 	step %{I wait for 5 seconds}
# 	login_button = query("* id:'dialog_option_login'")
# 	if login_button.length > 0
# 	  tap_when_element_exists("* dialog_option_signup")
# 	  wait_for_elements_do_not_exist("btn_join_now")
# 	  enter_text("* marked:'first_name'", "first")
# 	  enter_text("* marked:'last_name'", "last")
# 	  enter_text("* marked:'email'", "test@autotest.com")
# 	  enter_text("* marked:'password'", "111111")
# 	  enter_text("* marked:'postcode'", "2000")
# 	  tap_when_element_exists("* cb_t_and_c")
#     end
# end

When /^there_is_a_update_dialog_dismiss_it$/ do
  wait_for_elements_do_not_exist("android.widget.ProgressBar", timeout:30)
#   step %{I wait for 2 seconds}
  sleep(5)
  rateDialog = query("* marked:'dialog_action_c'")
  if rateDialog.length > 0
      puts "Close the rating dialog"
	  tap_when_element_exists("* marked:'dialog_action_c'")
  end
  
  update = query("* marked:'Update'")
  if update.length > 0
    puts "Close the service UPDATE dialog"
	tap_when_element_exists("* marked:'Update'")
  else
    puts "No UPDATE dialog"
  end
end

###########################################################################
#   					Methods for offer actions						  #
###########################################################################
Then(/^I_select_vertical "(.*?)"$/) do |verticalName|
  puts "PSS DEAL VERTICAL NAME = #{verticalName}"
  pss_vertical = query("TextView text:'#{verticalName}'")
  if pss_vertical.length > 0
  	puts "Found PSS vertical"
  	tap_when_element_exists("TextView text:'#{verticalName}'")
  else
  	puts "No PSS vertical"
  end
end

Then /^I_scroll_to_deal "([^\"]*)"/ do |dealName|
  puts "PSS DEAL NAME = #{dealName}"
  
  wait_for_element_exists("CudoFontTextView id:'tv_offer_price'", timeout:20)
  deal = query("com.aussiecommerce.cudo.ui.CudoFontTextView {text beginswith '#{dealName}'}")
  
  if deal.length < 1
    wait_poll(timeout:20,
            timeout_message:'Can not find target',
   	  		until_exists: "com.aussiecommerce.cudo.ui.CudoFontTextView {text beginswith '#{dealName}'}") do
  	  step %{I scroll down}
    end
  end
  puts "Found the deal"
end

Then /^I_select_deal "([^\"]*)"/ do |dealName|
  tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView {text beginswith '#{dealName}'}")
end

Then /^I_wait_deal_can_buy$/ do
  wait_for_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'buy_now'", timeout:20)
  check_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'buy_now'")
  puts "Deal is ready to buy"
end

# We should add logic to make sure the product is not sold out
Then /^I_select_a_product$/ do
  wait_for_element_exists("* id:'image_preview'", timeout:20)
  check_element_exists("* id:'image_preview'")
  tap_when_element_exists("* id:'image_preview' index:3")
  wait_for_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'Add to cart'", timeout:20)
  check_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'Add to cart'")
  puts "Found a product to buy"
end

# Then /^I tap offer (\d+)$/ do |index|
#   prices = query("com.aussiecommerce.cudo.ui.CudoFontTextView id:'tv_offer_price'")
#   if prices.length > index.to_i 
#   	puts "price found: #{prices.length}" 
#     tap_when_element_exists("* id:'tv_offer_price' index:#{index}")
#   else
#   	puts "index out of bound : #{index} > #{prices.length-1}"
#   end
# end

# select 'Add to cart' or 'Buy Now' on deal/product detail page.
Then /^I_add_item_to_cart$/ do
	buy_now = query("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'buy_now'")
	add_to_cart = query("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'btn_add_to_cart'")
	
	if(buy_now.length > 0)
		puts "The button is Buy Now"
		tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView id:'buy_now'")
	end
		
	if(add_to_cart.length > 0)
		puts "The button is Add To Cart"
		tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'btn_add_to_cart'")
	end
    wait_for_elements_do_not_exist("android.widget.ProgressBar", timeout:30)
end

Then (/^I_wait_for_progress_finish$/) do
    wait_for_elements_do_not_exist("android.widget.ProgressBar", timeout:30)
end

Then /^I_select_first_price_option$/ do
  wait_for_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'tv_option_seq'", timeout:5)
  check_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'add_to_cart'")
  tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'add_to_cart' index:0")
end

Then /^I_select_second_price_option$/ do
  wait_for_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'tv_option_seq'", timeout:5)
  check_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'add_to_cart'")
  tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'add_to_cart' index:1")
end

Then /^I_select_location$/ do
  wait_for_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'tv_location_name'", timeout:5)
  tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'buy_now' index:1")
end

Then /^I_select_a_parameter_if_needed$/ do
  param = query("* marked:'prod_param_value_grid'")
  if param.length > 0
    tap_when_element_exists("GridView CudoFontTextView index:0")
    step %{I_select_first_price_option}
  end
end

###########################################################################
#   					Methods for cart actions						  #
###########################################################################
Then /^I_clear_the_item_from_my_cart$/ do
  step %{I press view with id "action_cart"}
  wait_for_elements_do_not_exist("android.widget.ProgressBar", timeout:20)
  puts "wait for 5 seconds"
  step %{I wait for 5 seconds}
  puts "after waiting, checking item deleting"

  restoreCart = query("* marked:'Cart Expired'")
  if restoreCart.length > 0 
    tap_when_element_exists("* marked:'dialog_action_cancel'")
  else
    itemDelete = query("ImageView marked:'item_delete'")
    while itemDelete.length > 0 do
      tap_when_element_exists("ImageView marked:'item_delete'")
      tap_when_element_exists("* marked:'dialog_action_confirm'")
      wait_for_elements_do_not_exist("android.widget.ProgressBar", timeout:30)
      itemDelete = query("ImageView marked:'item_delete'")
    end
  end
  tap_when_element_exists("ImageButton marked:'Navigate up'")
#   wait_for_element_exists("ImageView marked:'item_delete'", timeout:20)
#   tap_when_element_exists("ImageView marked:'item_delete'")
#   wait_for_element_exists("* marked:'dialog_action_confirm'", timeout:3)
#   tap_when_element_exists("* marked:'dialog_action_confirm'")
end

Then /^I_go_to_my_cart$/ do
  step %{I press view with id "action_cart"}
  wait_for_element_exists("ImageView marked:'item_delete'", timeout:20)
end

Then /^I_start_making_payment$/ do
  tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontTextView marked:'buy_now'")
end

Then /^I_select_address_if_needed$/ do
#   when_element_exists("* id:'select_this_address' index:0", timeout:5)
  wait_for_element_exists("* id:'select_this_address'", timeout:5)
  tap_when_element_exists("* id:'select_this_address' index:0")
end

Then (/^I_pay_by_new_card$/) do
  wait_for_element_exists("com.aussiecommerce.cudo.ui.CudoFontButton marked:'use_another_cc_button'", timeout:20)
  tap_when_element_exists("com.aussiecommerce.cudo.ui.CudoFontButton marked:'use_another_cc_button'")
end

###########################################################################
#   					Methods for payment     						  #
###########################################################################
Then(/^I_create_new_credit_card number "(.*?)" expire month "(.*?)" expire year "(.*?)" cvc "(.*?)"$/) do |creditCardNumber, expireMonth, expireYear, cvc|
  wait_for_element_exists("CudoFontEditText marked:'card_number_edit'", timeout:20)
  enter_text("CudoFontEditText marked:'card_number_edit'", "'#{creditCardNumber}'")

  wait_for_element_exists("CudoFontEditText marked:'month_edit'", timeout:20)
  enter_text("CudoFontEditText marked:'month_edit'", "'#{expireMonth}'")

  wait_for_element_exists("CudoFontEditText marked:'year_edit'", timeout:20)
  enter_text("CudoFontEditText marked:'year_edit'", "'#{expireYear}'")

  wait_for_element_exists("CudoFontEditText marked:'cvc_edit'", timeout:20)
  enter_text("CudoFontEditText marked:'cvc_edit'", "'#{cvc}'")

  tap_when_element_exists("CudoFontButton marked:'save_card'")

#   wait_for_element_exists("CudoFontTextView {text beginswith 'Payment Success'}", timeout:20)
  wait_for_element_exists("CudoFontTextView marked:'OK'", timeout:20)
  tap_when_element_exists("CudoFontTextView marked:'OK'")
end

Then (/^I_pay_by_saved_card cvc "(.*?)"/) do |cvc|
  wait_for_element_exists("* marked:'use_saved_cc_button'", timeout:10)
  tap_when_element_exists("* marked:'use_saved_cc_button'")

  wait_for_element_exists("* marked:'expiry'", timeout:10)
  tap_when_element_exists("* marked:'expiry' index:0")

  wait_for_element_exists("CudoFontEditText marked:'cvc_edittext'", timeout:10)
  enter_text("CudoFontEditText marked:'cvc_edittext'", "'#{cvc}'")
  tap_when_element_exists("* marked:'dialog_action_pay'")
  
  wait_for_element_exists("CudoFontTextView marked:'OK'", timeout:20)
  tap_when_element_exists("CudoFontTextView marked:'OK'")
end

Then (/^I_pay_by_paypal/) do
  wait_for_element_exists("* marked:'paypal_button'", timeout:10)
  tap_when_element_exists("* marked:'paypal_button'")
  
  wait_for_elements_do_not_exist("android.widget.ProgressBar", timeout:30)
  
  dialog = query("* marked:'dialog_action_confirm'")
  if dialog.length > 0
    tap_when_element_exists("* marked:'dialog_action_confirm'")
  end
  sleep(5)

  wait_for_element_exists("* marked:'paypal_webview'", timeout:20)
  touch("webview css:'input[id=email]'")
  keyboard_enter_text("integration.test@gmail.com")

  touch("webview css:'input[id=password]'")
  keyboard_enter_text("integration.test")
  
  touch("webview css:'input[id=login]'")
  sleep(15)
  
  scroll("webview", :down)
  touch("webview css:'input[id=continue]'")
  
  wait_for_element_exists("* marked:'dialog_action_confirm'", timeout:10)
  tap_when_element_exists("* marked:'dialog_action_confirm'")
  
  tap_when_element_exists("* marked:'Navigate up'")
  tap_when_element_exists("* marked:'Navigate up'")
end

Then (/^I_add_new_address/) do
  tap_when_element_exists("* marked:'add_new_address_textview'", timeout:5)
  wait_for_element_exists("* marked:'save_address'", timeout:10)
  enter_text("* marked:'appt_unit_edit'", "201")
  enter_text("* marked:'shipping_address_edit'", "Kent st.")
  enter_text("* marked:'postcode_edit'", "2000")
  wait_for_element_exists("* marked:'select_dialog_listview'", timeout:10)
  tap_when_element_exists("* marked:'text1' index:0")
  enter_text("* marked:'state_edit'", "NSW")
  enter_text("* marked:'phone_edit'", "0423456789")
  tap_when_element_exists("* marked:'save_address'")
  wait_for_element_exists("* marked:'dialog_action_confirm'", timeout:10)
  tap_when_element_exists("* marked:'dialog_action_confirm'")
end

# Then (/^I_go_back_to_home_screen$/) do  
#   wait_for_element_exists("* marked:'active'", timeout:20)
#   step %{I go back}
# 
#   wait_for_elements_do_not_exist("* marked:'action_select_city'", timeout:20)
#   step %{I go back}
# 
#   wait_for_elements_do_not_exist("* marked:'action_select_city'", timeout:20)
#   step %{I go back}
# end